module gitlab.com/maxice8/meltryllis/src/alt-tab-daemon

go 1.17

require (
	github.com/TheCreeper/go-notify v0.2.0
	github.com/joshuarubin/go-sway v1.0.0
)

require (
	github.com/godbus/dbus/v5 v5.0.5 // indirect
	github.com/joshuarubin/lifecycle v1.0.0 // indirect
	go.uber.org/atomic v1.3.2 // indirect
	go.uber.org/multierr v1.1.0 // indirect
	golang.org/x/sync v0.0.0-20190412183630-56d357773e84 // indirect
)
