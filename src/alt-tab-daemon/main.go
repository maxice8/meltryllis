package main

import (
	"context"
	"errors"
	"fmt"
	"io/fs"
	"log"
	"net"
	"os"
	"sync"
	"sync/atomic"
	"time"

	"github.com/TheCreeper/go-notify"
	"github.com/joshuarubin/go-sway"
)

const (
	maxInputSize = 4                      // either "prev" or "next"
	deadline     = 200 * time.Millisecond // how long to perform actions
)

type info struct {
	// Mutex for accessing ids
	mu sync.RWMutex
	// List of IDs that are present in the workspace
	ids []int64
	// Which ID sway is currently focused on
	focusedID int64
}

var (
	state = &info{}

	// Log that goes to stdout
	outLog = log.New(os.Stdout, "", log.LstdFlags)
	// Log that goes to stderr
	errLog = log.New(os.Stderr, "", log.LstdFlags)
)

func main() {
	socket, err := createSocket()
	if err != nil {
		notif("failed to start", err.Error()) // notification
		errLog.Fatal(err)                     // log+exit
	}
	defer socket.Close()

	// errors from goroutines.
	errChan := make(chan error)

	// For every listener spawn this.
	go func(socket net.Listener) {
		for {
			c, err := socket.Accept()
			if err != nil {
				errChan <- err
				return
			}
			// Ignore any errors from here as they are non-fatal, the only
			// errors we want to be fatal are the ones above which indicate
			// the socket is not working properly
			go handleConnection(c)
		}
	}(socket)

	// context for our long-lived connections, this is used in the |state initialization|
	// and then for the |subscription|.
	ctx := context.Background()

	// |state initialization|
	client, err := sway.New(ctx)
	if err != nil {
		notif("init failed", errClientCreation.Error()) // notification
		errLog.Fatal(errClientCreation)                 // log+exit
	}

	err = state.getIDsAndFocused(client)
	if err != nil {
		notif("init failed", errStateInit.Error()) // notification
		errLog.Fatal(errStateInit)                 // log+exit
	}
	outLog.Printf("focused: %v\n", state.focused())
	outLog.Printf("%v\n", state.getIDs())

	handler := ourHandler{
		EventHandler: sway.NoOpEventHandler(), // handles events we don't want to.
		client:       client,                  // subscribe and perform any operations.
	}

	// Run the subscription in a goroutine.
	go func() {
		err = sway.Subscribe(ctx, handler, sway.EventTypeWindow, sway.EventTypeWorkspace)
		if err != nil {
			errChan <- err
		}
	}()

	// We started
	notif("initialized", "now running")

	// block for errors and exit as losing either the socket or the subscription is fatal
	// if we lose the socket then we can't switch focus as there is no socket listening
	// for commands, if we lose the subscription then we can't switch the focus in any
	// meaningful way as we don't have any way to keep a consistent view of the nodes
	// in a workspace.
	err = <-errChan
	if err != nil {
		errLog.Fatal(err)
	}
}

func handleConnection(socketConnection net.Conn) {
	defer socketConnection.Close()

	// We either receive 'prev' or 'next', no need to get a buffer bigger
	// than that, if someone sends something like 'previous' it will be
	// truncated to 'prev'
	recvBuffer := make([]byte, maxInputSize)

	// Read it into our buffer and deal with any errors
	_, err := socketConnection.Read(recvBuffer)
	if err != nil {
		errLog.Printf(`error reading socket: %v\n`, err)
		return
	}

	recv := string(recvBuffer)

	err = state.focus(recv)
	if err != nil {
		// Create an error notification as this is the result of an user
		// interaction with the socket
		notif("failed to switch focus", err.Error()) // notification
		errLog.Println(err)                          // log
	}
}

// setFocused takes an int64 and atomically sets |focusedID|
func (i *info) setFocused(newID int64) {
	atomic.StoreInt64(&i.focusedID, newID)
}

// focused atomically returns the value of |focusedID|
func (i *info) focused() int64 {
	return atomic.LoadInt64(&i.focusedID)
}

// appendIDs takes a slice of int64, acquires a read lock and appends to |ids|
func (i *info) appendIDs(ids ...int64) {
	i.mu.Lock()
	defer i.mu.Unlock()

	// Append all values we got
	i.ids = append(i.ids, ids...)
}

// setIDs takes a slice of int64s, acquires a read lock and then sets |ids|
func (i *info) setIDs(ids []int64) {
	i.mu.Lock()
	defer i.mu.Unlock()
	i.ids = ids
}

// getIDs gets a read-lock and returns |ids|
func (i *info) getIDs() []int64 {
	i.mu.RLock()
	defer i.mu.RUnlock()
	return i.ids
}

// removefromIDs acquires a lock and removes the first element that matches
// the given value from the IDs slice by splitting it
func (i *info) removefromIDs(id int64) {
	// Lock and defer unlocking so we don't run over other
	// operations
	i.mu.Lock()
	defer i.mu.Unlock()

	// find the id and split it away
	for x := range i.ids {
		if i.ids[x] == id {
			i.ids = append(i.ids[:x], i.ids[x+1:]...)
			return
		}
	}
}

func (i *info) focus(action string) error {
	// Get array
	array := i.getIDs()
	focused := i.focused()

	// Leave early if alt-tabbing with 1 or less ids
	if len(array) <= 1 {
		return errTriedSwitchInvalidWorkspace
	}

	// crate a good context for us to use.
	ctx, cancel := context.WithTimeout(context.Background(), deadline)
	defer cancel()

	// create a command client.
	commandClient, err := sway.New(ctx)
	if err != nil {
		return err
	}

	var target int64

	for x := range array {
		// If we find our position we want to get one more
		if array[x] == focused {
			switch action {
			case "next":
				// add 1 because we count from 0, check if we reach the limit
				if x+1 == len(array) {
					// our focused window is at the end of the slice and we want
					// to go to the next, so loop over to the start of the slice
					target = 0
				} else {
					// Add 1 from 0-counting
					target = int64(x) + 1
				}
			case "prev":
				if x == 0 {
					target = int64(len(array) - 1)
				} else {
					target = int64(x) - 1
				}
			default:
				return &unexpectedInputError{action}
			}
			// we don't care about the reply
			_, err := commandClient.RunCommand(
				ctx,
				fmt.Sprintf("[con_id=%d] focus", array[target]),
			)
			if err != nil {
				return fmt.Errorf("could not switch to %d: %w", array[target], err)
			}
		}
	}
	return nil
}

func createSocket() (net.Listener, error) {
	// Check if SESSION_DIR is set
	sessionDir, present := os.LookupEnv("SESSION_DIR")
	if !present {
		return nil, errSessionDirNotSet
	}

	if sessionDir == "" {
		return nil, errSessionDirEmpty
	}

	socketPath := fmt.Sprintf("%s/alt-tab-daemon", sessionDir)

	// Delete the socket if it exists, it should be ours
	err := os.Remove(socketPath)
	if err != nil && !errors.Is(err, fs.ErrNotExist) {
		return nil, &socketCreateError{err}
	}

	socket, err := net.Listen("unix", socketPath)
	if err != nil {
		return nil, err
	}
	return socket, nil
}

func (i *info) getIDsAndFocused(client sway.Client) error {
	activeOutput, focusedWorkspace, err := getActiveOutputAndWorkspace(client)
	if err != nil {
		return err
	}

	workspaceNodes, err := getWorkspaceNodes(client, activeOutput, focusedWorkspace)
	if err != nil {
		return err
	}

	// Store all IDs
	var IDs []int64

	for x := range workspaceNodes {
		node := workspaceNodes[x]
		IDs = append(IDs, node.ID)
		if node.Focused {
			i.setFocused(node.ID) // atomic
		}
	}
	i.setIDs(IDs) // locks for appending the array
	return nil
}

func getActiveOutputAndWorkspace(client sway.Client) (string, string, error) {
	ctx, cancel := context.WithTimeout(context.Background(), deadline)
	defer cancel()

	outputs, err := client.GetOutputs(ctx)
	if err != nil {
		return "", "", err
	}

	for x := range outputs {
		output := outputs[x]
		if output.Active {
			if output.CurrentWorkspace == "" {
				return "", "", errNoWorkspaceInActiveOutput
			}
			return output.Name, output.CurrentWorkspace, nil
		}
	}
	return "", "", errNoActiveOutput
}

func getWorkspaceNodes(
	client sway.Client,
	targetOutput string,
	targetWorkspace string,
) (
	nodes []*sway.Node,
	err error,
) {
	ctx, cancel := context.WithTimeout(context.Background(), deadline)
	defer cancel()

	tree, err := client.GetTree(ctx)
	if err != nil {
		return nil, err
	}

	output, err := getTargetNode(tree.Nodes, "output", targetOutput)
	if err != nil {
		return nil, errTargetOutputNotFound
	}

	workspace, err := getTargetNode(output.Nodes, "workspace", targetWorkspace)
	if err != nil {
		return nil, errTargetWorkspaceNotFound
	}

	var allNodes []*sway.Node
	if len(workspace.Nodes) != 0 {
		allNodes = append(allNodes, workspace.Nodes...)
	}
	if len(workspace.FloatingNodes) != 0 {
		allNodes = append(allNodes, workspace.FloatingNodes...)
	}
	return allNodes, nil
}

func getTargetNode(nodes []*sway.Node, _type string, target string) (*sway.Node, error) {
	for x := range nodes {
		node := nodes[x]

		if node.Type != _type || node.Name != target {
			continue
		}
		return node, nil
	}
	return nil, errTargetNodeNotFound
}

type ourHandler struct {
	sway.EventHandler
	client sway.Client // Client to handle subscription
}

func (h ourHandler) Window(ctx context.Context, e sway.WindowEvent) {
	switch e.Change {
	case "new":
		// New node created, add it to slices
		state.appendIDs(e.Container.ID)
		outLog.Printf("new: added node with ID: %v\n", e.Container.ID)
		outLog.Printf("new: now have: %v\n", state.getIDs())
	case "close", "move":
		// Node was closed, remove it from the slice of ids
		// Node was moved, we don't need to add it to the workspace
		// it was moved to because that will be refreshed once we switch
		// over to it, we just need to remove it from the current workspace
		state.removefromIDs(e.Container.ID)
		outLog.Printf("close: removed node with ID: %v\n", e.Container.ID)
		outLog.Printf("close: now have: %v\n", state.getIDs())
	case "focus":
		// if ID is not 0 then we are dealing with a new focused window inside the
		// as workspace
		state.setFocused(e.Container.ID)
		outLog.Printf("focus: now on: %v\n", e.Container.ID)
	case "title", "floating", "fullscreen_mode":
	// we don't want to do anything so just claim it is handled
	default:
		// Unhandled event
		errLog.Println(&unhandledEventError{"window", e.Change})
	}
}

func (h ourHandler) Workspace(ctx context.Context, e sway.WorkspaceEvent) {
	switch e.Change {
	case "focus":
		var err error
		err = state.getIDsAndFocused(h.client)
		if err != nil {
			errLog.Println(&refreshStateError{err})
			return
		}
		outLog.Printf("workspace: new IDs: %v\n", state.getIDs())
		outLog.Printf("workspace: now focused on: %v\n", state.focused())
	default:
		// Unhandled event
		errLog.Println(&unhandledEventError{"workspace", e.Change})
	}
}

func notif(summary string, body string) {
	// Create an error notification as this is the result of an user
	// interaction with the socket
	notification := notify.NewNotification(summary, body)
	notification.AppName = "alt-tab-daemon"
	if _, err := notification.Show(); err != nil {
		errLog.Printf("could not send notification: %v\n", err)
	}
}
