package main

import (
	"errors"
	"fmt"
)

var (

	// $SESSION_DIR is not set
	errSessionDirNotSet = errors.New("SESSION_DIR not set")
	// $SESSION_DIR Is set to an empty variable
	errSessionDirEmpty = errors.New("SESSION_DIR is empty")
	// no output is active
	errNoActiveOutput = errors.New("no output is active")
	// there is no workspace visible in the currently active output
	errNoWorkspaceInActiveOutput = errors.New("no workspace visible in the current active output")

	// could not find target output in the given node-tree
	errTargetOutputNotFound = errors.New("target output not found")
	// could not find target workspace in the given node-tree
	errTargetWorkspaceNotFound = errors.New("target workspace not found")
	// could not find node in the given node-tree
	errTargetNodeNotFound = errors.New("target node not found")

	// Received a "prev" or "next" while in a workspace that there is only one node or less
	errTriedSwitchInvalidWorkspace = errors.New("tried switch in workspace with 1 or less nodes")

	// can't create the sway client
	errClientCreation = errors.New("client creation failed")
	// can't get IDs and focused ID in the startup
	errStateInit = errors.New("could not get IDs and focused")
)

// We received an unexpected input, we expect either "next" or "prev"
type unexpectedInputError struct {
	action string
}

func (e *unexpectedInputError) Error() string {
	return fmt.Sprintf(`unexpected string "%s", expected "next" or "prev"`, e.action)
}

// We could not create the socket, this wraps the error that triggered it
type socketCreateError struct {
	err error
}

func (e *socketCreateError) Error() string {
	return fmt.Sprintf(`could not create socket: %v`, e.err)
}

// We could not refresh the global state (ids, focused_id), wraps the actual error
type refreshStateError struct {
	err error
}

func (e *refreshStateError) Error() string {
	return fmt.Sprintf(`could not refresh state: %v`, e.err)
}

// we received an event we do not handle
type unhandledEventError struct {
	subscription string
	event        string
}

func (e *unhandledEventError) Error() string {
	return fmt.Sprintf(`unhandled "%s" event: %s`, e.subscription, e.event)
}
