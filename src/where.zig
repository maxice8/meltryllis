const std = @import("std");

const WhereErrors = error{
    NotEnoughArgs,
    NotFound,
    PathNotSet,
};

const ExitCode = enum(u8) {
    Success = 0, // We found the binary and printed it to the user
    NotFound = 1, // Binary not found
    PathNotSet = 99, // $PATH is not set
    NotEnoughArgs = 100, // No args were given
    Syscall = 111, // A syscall failed (most cases out of memory)
};

// u8 is the type accepted for std.process.exit
// an error is returned only if a syscall we expected to
// always work like printing to stdout/stderr or allocating
// a path to PATH_MAX
fn run() ![]u8 {
    // Check if we have an element in argv
    const args = std.os.argv;
    if (args.len < 2) {
        return error.NotEnoughArgs;
    }

    // Buffer, we use PATH_MAX since we are handling paths
    var buffer: [std.c.PATH_MAX]u8 = undefined;
    var fba = std.heap.FixedBufferAllocator.init(&buffer);
    const allocator = &fba.allocator;

    // get the value of the PATH environment variable
    // if PATH is unset (for some reason) return exit code 99
    const path = std.os.getenv("PATH") orelse return error.PathNotSet;

    // Split our PATH by ':'
    var splitPath = std.mem.split(path, ":");

    while (splitPath.next()) |elem| {
        // Concatenate the PATH/PROGRAM
        const filepath = try std.fmt.allocPrint(allocator, "{s}/{s}", .{ elem, args[1] });

        // Check if we have executable permissions and is file, we are not doing
        // any operations on it an djust returning it to the user
        std.os.access(filepath, std.c.F_OK | std.c.X_OK) catch continue;

        return filepath;
    }
    return error.NotFound;
}

pub fn main() !u8 {
    const file = run() catch |err| switch (err) {
        // errors are sorted in ascending order of exit code.
        error.NotFound => return @enumToInt(ExitCode.NotFound),
        error.PathNotSet => return @enumToInt(ExitCode.PathNotSet),
        error.NotEnoughArgs => {
            // write usage to stderr
            const stderr = std.io.getStdErr().writer();
            try stderr.writeAll("usage: where program\n\tsearch program in $PATH and print it\n");
            return @enumToInt(ExitCode.NotEnoughArgs);
        },
        error.OutOfMemory => return @enumToInt(ExitCode.Syscall),
    };

    // a writer to stdout
    const stdout = std.io.getStdOut().writer();

    // write and return 0
    try stdout.print("{s}\n", .{file});
    return @enumToInt(ExitCode.Success);
}
