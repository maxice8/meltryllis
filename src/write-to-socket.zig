const std = @import("std");

const UsageError = error{
    /// Missing first argument, path to the socket.
    MissingSocket, 
    /// Missing second argument, the message to be written to the socket.
    MissingMessage,
};

const ExitCode = enum(u8) {
    Success = 0,
    Failed = 1,
    Syscall = 111,

    /// Exit codes related to args we receive, not internal operation.
    const Usage = enum(u8) {
        Socket = 100,
        Message = 101,
    };
};

/// Usage message
const usage = 
    \\usage: write-to-socket socket message
    \\    write message to socket
    ;

/// take an allocator and return a slice of 2 strings taken from commandline
/// arguments, the first value is the path of the socket, the second is the
/// message to be written.
///
/// If the first arg is missing then error.MissingSocket is returned, if
/// the second is missing the error.MissingMessage is returned.
fn get_socket_and_message(allocator: *std.mem.Allocator) ![2][]const u8 {
    // must be `var` as we process the args
    var args = std.process.args();

    // skip $0th arg, no need to bother error checking since $0 is
    // always filled
    _ = args.skip();

    // the try will return either the error when allocating
    // or `NotEnoughArgs` which will print the usage message
    const socket_path = try (args.next(allocator) orelse {
        return error.MissingSocket;
    });
    const message = try (args.next(allocator) orelse {
        return error.MissingMessage;
    });

    return [2][]const u8{ socket_path, message };
}

/// Take |path| and |message|, both strings ([]const u8) and tries to
/// connect to the unix socket in |path| and write |message| to it
fn write(path: []const u8, message: []const u8) !void {
    // Try to open the socket
    const stream = try std.net.connectUnixSocket(path);
    // write the message, ignore any returns
    _ = try stream.write(message);
}

pub fn main() !u8 {
    // allocator
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = &arena.allocator;

    const res = get_socket_and_message(allocator) catch |err| switch (err) {
        error.OutOfMemory => return @enumToInt(ExitCode.Syscall),
        error.MissingSocket => {
            const stderr = std.io.getStdErr().writer();
            try stderr.print("{s}\n", .{usage});
            return @enumToInt(ExitCode.Usage.Socket);
        },
        error.MissingMessage => {
            const stderr = std.io.getStdErr().writer();
            try stderr.print("{s}\n", .{usage});
            return @enumToInt(ExitCode.Usage.Message);
        },
        error.InvalidCmdLine => {
            const stderr = std.io.getStdErr().writer();
            try stderr.print("write-to-socket: {}\n", .{err});
            return @enumToInt(ExitCode.Failed);
        },
    };
    const socket = res[0];
    const message = res[1];

    write(socket, message) catch |err| switch (err) {
        error.BrokenPipe => {
            const stderr = std.io.getStdErr().writer();
            try stderr.print("write-to-socket: write {s}: broken pipe\n", .{socket});
            return @enumToInt(ExitCode.Failed);
        },
        error.FileNotFound => {
            const stderr = std.io.getStdErr().writer();
            try stderr.print("write-to-socket: write {s}: not found\n", .{socket});
            return @enumToInt(ExitCode.Failed);
        },
        error.ConnectionRefused => {
            const stderr = std.io.getStdErr().writer();
            try stderr.print("write-to-socket: write {s}: connection refused\n", .{socket});
            return @enumToInt(ExitCode.Failed);
        },
        error.ConnectionTimedOut => {
            const stderr = std.io.getStdErr().writer();
            try stderr.print("write-to-socket: write {s}: timeout\n", .{socket});
            return @enumToInt(ExitCode.Failed);
        },
        error.PermissionDenied => {
            const stderr = std.io.getStdErr().writer();
            try stderr.print("write-to-socket: write {s}: permission denied\n", .{socket});
            return @enumToInt(ExitCode.Failed);
        },
        else => {
            const stderr = std.io.getStdErr().writer();
            try stderr.print("write-to-socket: write {s}: {} (REPORT UPSTREAM)\n", .{ socket, err });
            return @enumToInt(ExitCode.Failed);
        },
    };
    return @enumToInt(ExitCode.Success);
}
