FAQ
===

#### Why the empty directories with empty .tree files ?

They are used by the makefiles to create the tree in my home, only files are symlinked.

TREE
====

```
.
├── bin
├── .cache
├── .var
├── .config
│   ├── alacritty
│   ├── dash
│   ├── fish
│   ├── git
│   ├── gtk-3.0
│   ├── gtk-4.0
│   ├── newsboat
│   └── nvim
├── usr
│   ├── Camera
│   ├── Images
│   ├── Music
│   ├── backup
│   ├── screenshots
│   ├── secrets
│   ├── src
│   ├── videos
│   └── walls
└── .local
    ├── log
    └── share
```

LICENSE
=======

GPL-3.0-or-later

COLORS
======

- #E53344
- #FFDDDD
