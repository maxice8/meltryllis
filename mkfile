MKSHELL=rc
LN=ln -sfvr
MKDIR=mkdir -pv
NPROC=`{nproc}
GITDIR=$PWD

# Split this as this is almost always good to have
GO_LDFLAGS= -ldflags='-s -w' -trimpath

# name of the go binary, so it can be overriden if someone
# wants to compile with another one
GO=go

# Universal variable that makes go compile statically
CGO_ENABLED=0

# Name of the zig binary, can be overriden
ZIG=zig

# some defaults flags, ZIGFLAGS can be used and they will
# override these ones
ZIG_COMP_FLAGS=-O ReleaseSafe --strip --cache-dir /tmp

# Default target, creates directories and links everything
default:QV: links mkdirs submodules build/src

# creates tree of directories relative to $HOME
mkdirs:QV: mkdir-local mkdir-config mkdir-usr

mkdir-local:V:
	find .local -type d -print0 \
		| xargs -0 -P $NPROC -I '{}' $MKDIR $HOME/'{}'

mkdir-config:V:
	find .config -type d -print0 \
		| xargs -0 -P $NPROC -I '{}' $MKDIR $HOME/'{}'

mkdir-usr:V:
	find usr -type d -print0 \
		| xargs -0 -P $NPROC -I '{}' $MKDIR $HOME/'{}'

links:QV: link-config link-bins link-man

# depend on creation of $HOME/.config before trying to link
link-config: mkdir-config
	$LN $PWD/profile $HOME/.profile
	find .config -type f -print0 -o -type l -print0  \
		| xargs -0 -P $NPROC -I '{}' $LN $PWD/'{}' $HOME/'{}'

link-bins:V:
	if(test ! -d $HOME/bin) mkdir -p $HOME/bin
	find bin -type f -print0 -o -type l -print0 \
		| xargs -0 -P $NPROC -I '{}' $LN $PWD/'{}' $HOME/'{}'

link-man:V: mkdir-local
	find .local/share/man -type f -print0 \
		| xargs -0 -P $NPROC -I '{}' $LN $PWD/'{}' $HOME/'{}'

build/src:V: \
	alt-tab-daemon \
	notify-ready \
	write-to-socket \
	human-size \
	where

alt-tab-daemon: src/alt-tab-daemon/main.go
	@ {
		cd src/alt-tab-daemon
		$GO build -v -o $HOME/bin/alt-tab-daemon $GO_LDFLAGS $GOFLAGS
	}

notify-ready:
	GOBIN=$HOME/bin $GO install $GO_LDFLAGS $GOFLAGS \
		gitlab.com/maxice8/readyfd/cmd/notify-ready@latest

write-to-socket: src/write-to-socket.zig
	@ {
		cd $HOME/bin
		$ZIG build-exe $GITDIR/$prereq $ZIG_COMP_FLAGS $ZIGFLAGS
	}

human-size: src/human-size.zig
	@ {
		cd $HOME/bin
		$ZIG build-exe $GITDIR/$prereq $ZIG_COMP_FLAGS $ZIGFLAGS
	}

where: src/where.zig
	@ {
		cd $HOME/bin
		$ZIG build-exe $GITDIR/$prereq $ZIG_COMP_FLAGS $ZIGFLAGS
	}

submodules:V:
	git submodule update --init --recursive
