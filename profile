#!/bin/sh
# Global environment variables, affects everything
# including all services ever started
export PATH="$HOME/bin":"$HOME"/.local/bin:"$PATH"
export TZ=Brazil/East
export ENV=$HOME/.config/dash/env
. "$HOME/.cargo/env"

case "$(tty)" in
	/dev/tty1) 
		exec startup --user
		;;
esac

