#!/usr/bin/env rc
# SPDX-License-Identifier: GPL-3.0-only
# mgbr BRANCH-NAME ... - mgbr merge branches
if(!git-intree) exit 1
if(~ $#* 0) *=(`{git branch --show-current})

for(branch in $*) {
	if(! git show-ref --quiet refs/heads/$branch) {
		printerr branch ''''$branch'''' does not exist
		exit 1
	}
}

if(! git diff --quiet) {
	printerr tree is dirty, can not continue
	exit 1
}

fn merge_branch {
	earlyleave=0
	
	prefix=`{tracking-branch $1}

	if(! gbr $prefix) return 1
	
	if(! git merge --quiet $* --no-edit --ff) {
		printerr merge: dropping to $SHELL, please fix or exit 1
		if(! $SHELL -l) {
			earlyleave=1
		}
	}

	if(! PRINTOK_QUIET=yes pullp) {
		printerr pull: dropping to $SHELL, please fix or exit 1
		if(! $SHELL -l) {
			earlyleave=1
		}
	}

	if(~ $earlyleave 1) {
		printerr failed to merge ''''$"*'''' into ''''$prefix'''', resetting
		git reset --hard ''''$prefix''''
		return 1
	}
	pushes=($pushes $prefix:$prefix)
}

pushes=()
branches=$*

edge=()
stable314=()
stable313=()
stable312=()
stable311=()
stable310=()

# Parse every branch given to us and separate them if they are meant for
# different branches of alpine, like 3.10-stable.
for(branch in $*) {
	switch(`{alpine-stable-prefix $branch}) {
	case 3.14
		stable314=($stable314 $branch)
	case 3.13
		stable313=($stable313 $branch)
	case 3.12
		stable312=($stable312 $branch)
	case 3.11
		stable311=($stable311 $branch)
	case 3.10
		stable310=($stable310 $branch)
	case *
		edge=($edge $branch)
	}
}

if(! ~ $#edge 0) merge_branch $edge
if(! ~ $#stable314 0) merge_branch $stable314
if(! ~ $#stable313 0) merge_branch $stable313
if(! ~ $#stable312 0) merge_branch $stable312
if(! ~ $#stable311 0) merge_branch $stable311
if(! ~ $#stable310 0) merge_branch $stable310

# Push our local changes to the upstream repo, we don't
# provide any info to pushp because it can detect that we
# are using 'upstream'
if(pushp $pushes) {
	dlbr $*
} else {
	printerr failed to push the following: $pushes
	exit 1
}
exit 0
