-- Helper function to set keymaps
local opts = { noremap = false, silent = true }

-- Fast save and exit
vim.api.nvim_set_keymap('', 'x', ':wq!<cr>', opts)

-- Fast save
vim.api.nvim_set_keymap('', '<Enter>', ':w<cr>', opts)

-- Fast exit
vim.api.nvim_set_keymap('', 'q', ':quit<cr>', opts)

-- Toggle paste mode on and off
vim.api.nvim_set_keymap('', '<Leader>p', ':setlocal paste!<cr>', opts)

vim.api.nvim_exec(
[[
function! Syntax_query() abort
  for id in synstack(line('.'), col('.'))
    echo synIDattr(id, 'name')
  endfor
endfunction
]], true)

vim.api.nvim_set_keymap('', '<C-s>q', ':call Syntax_query()<cr>', { noremap = false, silent = true })
