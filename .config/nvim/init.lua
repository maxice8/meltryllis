-- Set a sane leader here
vim.g.mapleader = ' '

-- Theme
Theme = {}

-- Helper function
Utils = {}

Utils.highlight = function(group, highlights)
	local fg = highlights[1] and " guifg=" .. highlights[1] or " guifg=NONE"
	local bg = highlights[2] and " guibg=" .. highlights[2]  or " guibg=NONE"
	local attr = highlights[3] and " gui=" .. highlights[3] or " gui=NONE"
	local sp = highlights[4] and " guisp=" .. highlights[4] or ""
	vim.api.nvim_command("highlight " .. group .. fg .. bg .. attr .. sp)
end

Utils.link = function(higroup, link_to, force)
  vim.cmd(
		string.format(
			[[highlight%s link %s %s]],
			force and "!" or " default",
			higroup,
			link_to)
		)
end

-- Sensible defaults
require('settings')

-- Keymaps
require('keymaps')

-- Return to last edit position when opening files (You want this!)
vim.api.nvim_exec(
[[
augroup reopen
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
]], false)


-- Install packer
local install_path = vim.fn.stdpath 'data' .. '/site/pack/packer/start/packer.nvim'

if vim.fn.empty(vim.fn.glob(install_path)) > 0 then
  vim.fn.execute('!git clone https://github.com/wbthomason/packer.nvim ' .. install_path)
end

vim.api.nvim_exec(
[[
augroup Packer
autocmd!
autocmd BufWritePost init.lua PackerCompile profile=true
augroup end
]], false)

local packer = require('packer')
local use = packer.use

packer.init {
	display = {
		open_fn = function()
			return require('packer.util').float {border = 'single'}
		end
	}
}

packer.startup(function()
  -- Support for scdoc, used to write manapges
  use {
		'gpanders/vim-scdoc',
		ft = {'scdoc'}
  }

  -- It is not on GitHub so we need full path, also there is no 'for'
  -- because it is the ftype required
  use {
  	'~/Repositories/apkbuild.vim',
		ft = {'apkbuild'}
  }

	use {
		'windwp/nvim-autopairs',
		after = 'nvim-compe',
		config = function()
			require('nvim-autopairs').setup({
				check_ts = true,
			})
			require('nvim-autopairs.completion.compe').setup({
				map_cr = true,
				map_complete = true -- Insert () func completion
			})
		end
	}

	use { -- show which lines are added, changed, removed also allow basic operations on them
		'lewis6991/gitsigns.nvim',
		requires = { 'nvim-lua/plenary.nvim' },
		after = 'vim-paper',
		config = function()
			require('gitsigns').setup {
				signs = {
				  add = {
				      hl = 'GitSignsAdd',
				      text = '▌',
				      numhl = 'GitSignsAddNr'
				  },
				  change = {
				      hl = 'GitSignsChange',
				      text = '▌',
				      numhl = 'GitSignsChangeNr'
				  },
				  delete = {
				      hl = 'GitSignsDelete',
				      text = '▌',
				      numhl = 'GitSignsDeleteNr'
				  },
				  topdelete = {
				      hl = 'GitSignsDelete',
				      text = '▌',
				      numhl = 'GitSignsDeleteNr'
				  },
				  changedelete = {
				      hl = 'GitSignsChange',
				      text = '▌',
				      numhl = 'GitSignsChangeNr'
				  }
				},
				numhl = false,
				watch_gitdir = {
				    interval = 100
				},
				sign_priority = 6,
				diff_opts = {internal = true},
				status_formatter = nil,
				keymaps = {
					-- Default keymap options
					noremap = true,
					buffer = true,

   	 			['n ]c'] = { expr = true, "&diff ? ']c' : '<cmd>lua require\"gitsigns\".next_hunk()<CR>'"},
   	 			['n [c'] = { expr = true, "&diff ? '[c' : '<cmd>lua require\"gitsigns\".prev_hunk()<CR>'"},

   	 			['n <C-g>s'] = '<cmd>lua require"gitsigns".stage_hunk()<CR>',
   	 			['n <C-g>u'] = '<cmd>lua require"gitsigns".undo_stage_hunk()<CR>',
   	 			['n <C-g>r'] = '<cmd>lua require"gitsigns".reset_hunk()<CR>',
   	 			['n <C-g>p'] = '<cmd>lua require"gitsigns".preview_hunk()<CR>',
   	 			['n <C-g>b'] = '<cmd>lua require"gitsigns".blame_line()<CR>',
				}
			}
		end
	}

  use { -- framework and collection of linters and fixers
    'dense-analysis/ale',
    ft = {'sh', 'bash', 'markdown', 'apkbuild', 'vim', 'cpp'},
    cmd = 'ALEEnable',
    setup = function()
			vim.g.ale_completion_enabled = true
			vim.g.ale_fix_on_save = false
		end,
		config = function()
			vim.api.nvim_set_keymap('n', '<C-f>', '<cmd>ALEFix<CR>', { noremap = false, silent = true })
			vim.cmd[[ALEEnable]]
		end
  }

	use { -- Nice bar
  	'nvim-lualine/lualine.nvim',
  	requires = {'kyazdani42/nvim-web-devicons', opt = true},
		config = function()
			Theme.lualine = {}
			local theme = Theme.lualine

			local colors = {
			  black        = "#000000",
			  white        = '#ffffff',
			  gray         = '#f7f3e3',
			  darkgray     = '#f2eede',
			  red          = '#ffeaea',
			  green        = '#55aa55',
			  blue         = '#9eeeee',
			  lightblue    = '#eaffff',
			  yellow       = '#cccc88',
			  violet       = '#8888cc',
			}

			theme.normal = {
			  b = {fg = colors.darkgray, bg = colors.gray},
			  a = {fg = colors.black, bg = colors.lightblue, gui = 'bold'},
			  c = {fg = colors.black, bg = colors.gray},
			}

			theme.visual = {
			  a = {fg = colors.black, bg = colors.lightblue, gui = 'bold'},
			  b = {fg = colors.black, bg = colors.violet},
			}

			theme.inactive = {
			  b = {fg = colors.darkgray, bg = colors.white},
			  a = {fg = colors.darkgray, bg = colors.gray, gui = 'bold'},
			}

			theme.replace = {
			  b = {fg = colors.black, bg = colors.yellow},
			  a = {fg = colors.black, bg = colors.yellow, gui = 'bold'},
			  c = {fg = colors.black, bg = colors.gray},
			}

			theme.insert = {
			  b = {fg = colors.darkgray, bg = colors.gray},
			  a = {fg = colors.black, bg = colors.blue, gui = 'bold'},
				c = {fg = colors.black, bg = colors.gray}
			}

			require('lualine').setup{
				options = {
					icons_enabled = false,
					theme = Theme.lualine,
				},
				extensions = { 'nvim-tree' },
				sections = {
					lualine_a = {'mode'},
					lualine_b = {'branch'},
					lualine_c = {
						'filename',
						{
							'diagnostics',
							sources = { 'nvim_lsp', 'ale' }, -- Only 2 sources we use
						}
					},
					lualine_x = {'encoding', 'fileformat', 'filetype'},
					lualine_y = {'diff', 'progress'},
					lualine_z = {'location'}
				},
				inactive_sections = {
					lualine_a = {},
					lualine_b = {},
					lualine_c = {'filename'},
					lualine_x = {'location'},
					lualine_y = {},
					lualine_z = {}
				},
				tabline = {}
			}
		end
	}

	use { -- automatically create missing directories when saving files, like `mkdir -p`
		'jghauser/mkdir.nvim',
		event = 'BufWrite',
		config = function() require('mkdir') end
	}

	use { -- Automatically change working directory to the root of the repo
		'airblade/vim-rooter',
		config = function()
			vim.g.rooter_patterns = {'.venv', '.git/', '.nvim/'}
		end
	}

	use { -- file manager
		'kyazdani42/nvim-tree.lua',
		requires = { 'kyazdani42/nvim-web-devicons' },
		wants = 'barbar.nvim',
		setup = function()
			-- Store the function in our global (_G) table, put it inside
			-- a table called '_helperfuncs', this is done so the keybind
			-- can be called, if we define the function locally we are out
			-- of luck
			_G._helperfuncs = {}
			_G._helperfuncs.toggle_tree = function()
				if require('nvim-tree.view').win_open() then
				  require('nvim-tree').close()
				  require('bufferline.state').set_offset(0)
				else
				  require('bufferline.state').set_offset(31, 'File Explorer')
				  require('nvim-tree').find_file(true)
				end
			end,
			vim.api.nvim_set_keymap(
				'n',
				'<C-a>',
				"<CMD>lua _G._helperfuncs.toggle_tree()<CR>",
				{ noremap = false, silent = true })
		end,
		config = function()
			require('nvim-tree').setup {
				disable_netrw = true,
				hijack_netrw = true,
				auto_close = true
			}
		end
	}

	use { -- tabline
		'romgrk/barbar.nvim',
		requires = { 'kyazdani42/nvim-web-devicons' },
		config = function()
			local map = vim.api.nvim_set_keymap
			local opts = { noremap = false, silent = true }
			map('n', '<C-j>', ':BufferPrevious<CR>', opts)
			map('n', '<C-l>', ':BufferNext<CR>', opts)
			map('n', '<C-k>', ':BufferClose<CR>', opts)
			-- use vim.api.nvim_exec because I can't get this to work with pure lua
			vim.api.nvim_exec(
				[[
				let bufferline = get(g:, 'bufferline', {})
				" no animations, they are choppy
				let bufferline.animation = v:false 
				" don't show tabs when there is only one buffer
				let bufferline.auto_hide = v:true
				" Don't use icons, just show the number of the buffer
				let bufferline.icons = v:"numbers"
				]],
			false)
		end
	}

	use { -- Show indentation levels
		'lukas-reineke/indent-blankline.nvim',
		event = 'BufRead',
		after = 'vim-paper',
		config = function()
			require('indent_blankline').setup{
				space_char_blankline = " ",
				show_current_context = true,
				filetype_exclude = {"help", "terminal", "dashboard"},
				buftype_exclude = {"terminal"}
			}
		end
	}

	use { -- Completion
		'hrsh7th/nvim-compe',
		event = 'InsertEnter',
		config = function()
			vim.o.completeopt = "menuone,noselect"
			require('vendor.nvim-compe')
		end -- Use the file we vendor from upstream
	}

	use {
    'neovim/nvim-lspconfig'
	}

	use {
    'williamboman/nvim-lsp-installer',
		config = function()
			local lsp_installer_servers = require('nvim-lsp-installer.servers')
			
			local servers = {
			    "sumneko_lua",
			    "zls",
			    "gopls",
					"vimls",
					"yamlls",
					"dockerls",
					"bashls",
					"awk_ls",
					"taplo"
			}
			
			-- Loop through the servers listed above.
			for _, server_name in pairs(servers) do
			    local server_available, server = lsp_installer_servers.get_server(server_name)
			    if server_available then
			        server:on_ready(function ()
			            -- When this particular server is ready (i.e. when installation is finished or the server is already installed),
			            -- this function will be invoked. Make sure not to use the "catch-all" lsp_installer.on_server_ready()
			            -- function to set up servers, to avoid doing setting up a server twice.
			            local opts = {}
			            server:setup(opts)
			        end)
			        if not server:is_installed() then
			            -- Queue the server to be installed.
			            server:install()
			        end
			    end
			end
		end
	}

	use { -- Show signature of a function as you write its arguments
		'ray-x/lsp_signature.nvim',
		wants = 'nvim-lspconfig'
	}

	use { -- Pretty list for showing LSP diagnostics
		'folke/trouble.nvim',
		wants = 'nvim-lspconfig',
		requires = {
			{
				'kyazdani42/nvim-web-devicons',
				opt = true
			},
			'folke/lsp-colors.nvim'
		},
		config = function()
			require('lsp-colors').setup{
				icons = false,
			}
			vim.api.nvim_set_keymap('n', '<C-q>', '<CMD>TroubleToggle<CR>', { silent = true, noremap = false })
		end
	}

	use {
		'vim-scripts/rcshell.vim'
	}

	use {
		'terminalnode/sway-vim-syntax'
	}

	use {
		'YorickPeterse/vim-paper',
		config = function()
			vim.o.termguicolors = true
			vim.cmd[[colorscheme paper]]
			local colors = {
				-- Taken from acme.nvim
				greenMid = '#448844',
				greenLight = '#88cc88',
				blueMid = '#00aaff',
				blueLight = '#cceeff',
				redMid = '#ffaaaa',
				redLight = '#bb5d5d',

				-- Taken from vim-paper
				background = '#f2eede',
				lightBackground = '#f7f3e3',

				lightGray1 = '#d8d5c7',
				lightGray2 = '#bfbcaf',
				lightGray3 = '#aaaaaa',

				gray = '#777777',
				green = '#216609',
				red = '#cc3e28',
				purple = '#5c21a5',
			}
			local syntax = {
				-- GitSigns support
				GitSignsAdd = { colors.greenMid, colors.greenLight, 'none', nil},
				GitSignsAddLn = { colors.greenMid, colors.greenLight, 'none', nil},
				GitSignsChange = { colors.blueMid, colors.blueLight, 'none', nil},
				GitSignsChangeLn = { colors.blueMid, colors.blueLight, 'none', nil},
				GitSignsDelete = { colors.redMid, colors.redLight, 'none', nil},
				GitSignsDeleteLn = { colors.redMid, colors.redLight, 'none', nil},

				-- IndentBlankLine
				IndentBlanklineContextChar = { colors.lightGray, nil, 'none', nil},
				IndentBlanklineChar = { colors.lightGray3, nil, 'nocombine', nil},
				IndentBlanklineSpaceChar = { colors.lightGray3, nil, 'nocombine', nil},

				-- Git support
				gitcommitHeader = { colors.gray, nil, 'none', nil},
				gitcommitOnBranch = { colors.gray, nil, 'none', nil},
				gitcommitBranch = { colors.black, nil, 'none', nil},
				gitcommitComment = { colors.gray, nil, 'none', nil},
				gitcommitSelectedType = { colors.green, nil, 'none', nil},
				gitcommitSelectedFile = { colors.green, nil, 'none', nil},
				gitcommitDiscardedType = { colors.red, nil, 'none', nil},
				gitcommitDiscardedFile = { colors.red, nil, 'none', nil},
				gitcommitBlank = { colors.black, nil, 'none', nil},

				-- Treesitter
				TSNamespace = { colors.black, nil, 'none', nil},
				TSParameter = { colors.black, nil, 'none', nil},
				TSParameterReference = { colors.black, nil, 'none', nil},
				TSConstructor = { colors.black, nil, 'none', nil},
				TSVariable = { colors.black, nil, 'none', nil},
				TSVariableBuiltin = { colors.black, nil, 'none', nil},
				TSText = { colors.black, nil, 'none', nil},
				TSStrong = { nil, nil, 'bold', nil},
				TSURI = { nil, nil, 'undercurl', nil},
			}
			for group, color in pairs(syntax) do
				Utils.highlight(group, color)
			end
			local links = {
				TSError = {'ErrorMsg', nil},
				TSString = {'String', nil},
				TSStringEscape = {'String', nil},
				TSCharacter = {'String', nil},
				TSKeyword = {'Keyword', nil},
				TSKeywordFunction = {'Keyword', nil},
				TSKeywordOperator = {'Keyword', nil},
				TSKeywordReturn = {'Keyword', nil},
				TSRepeat = {'Keyword', nil},
				TSLabel = {'Keyword', nil},
				TSNumber = {'Number', nil},
				TSFloat = {'Number', nil},
				TSType = {'Keyword', nil},
				TSTypeBuiltin = {'Keyword', nil},
				TSConstMacro = {'Macro', nil},
				TSFuncMacro = {'Macro', nil},
				TSFuncBuiltin = {'Function', nil},
				TSFunction = {'Function', nil},
				TSStringRegex = {'Regexp', nil},
				TSComment = {'Comment', nil},
				TSOperator = {'Operator', nil},
				TSPunctDelimiter = {'Special', nil},
				TSPunctBracket = {'Special', nil},
				TSPunctSpcial = {'Special', nil},
				TSConstant = {'Constant', nil},
				TSConstBuiltin = {'Constant', nil},
				TSInclude = {'Include', nil},
				TSField = {'Directory', nil},
				TSProperty = {'Directory', nil},
				TSAttribute = {'Identifier', nil},
				TSTag = {'Identifier', nil},
				TSBoolean = {'Boolean', nil},
				TSMethod = {'Function', nil},
				TSConditional = {'Keyword', nil},
				TSException = {'Keyword', nil},
				TSStructure = {'Structure', nil},
			}
			for fromgroup, togroup_and_force in pairs(links) do
				Utils.link(fromgroup, togroup_and_force[1], togroup_and_force[2])
			end
		end
	}

	use {
		'nvim-treesitter/nvim-treesitter',
		event = 'BufRead',
		config = function()
			require('nvim-treesitter.configs').setup {
				autopairs = {enable = true},
				ensure_installed = {
					'lua',
					'c',
					'cpp',
					'go',
					'gomod',
					'fish',
					'json',
					'vim',
					'yaml',
					'zig',
					'toml'
				},
				highlight = {
					enable = true,
					use_languagetree = true,
				}
			}
		end
	}
end)
