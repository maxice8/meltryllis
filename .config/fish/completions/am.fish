# Repositories
set -l repositories main community testing unmaintained non-free

# Offer repositories to move and rename
complete -f -c am -n "__fish_al_move_ask_repo" -a "$repositories"
complete -f -c am -n "__fish_al_move_ask_aport" -a "(__fish_al_move_valid_repos $repositories)"

# grab all packages
function __fish_atools_get_aports
	for repo in $argv
		ls $APORTSDIR/$repo | string replace -r '$' "\t$repo"
	end
end

# al is very strict, make sure the first argument to the 'move' subcommand
# is *ALWAYS* a repo
function __fish_al_move_ask_repo
	set -l cmd (commandline -poc)
	[ (count $cmd) -gt 1 ] && return 1
	[ (count $cmd) -lt 1 ] && return 1
	return 0
end

# same as above but guaranteed the second is the name of the aport we will
# be moving
function __fish_al_move_ask_aport
	set -l cmd (commandline -poc)
	[ (count $cmd) -gt 2 ] && return 1
	[ (count $cmd) -lt 2 ] && return 1
	return 0
end

# return the valid repos
function __fish_al_move_valid_repos
	set -l cmd (commandline -poc)
	set -l repo $cmd[1]
	set -l index (contains -i -- $repo $argv)
	if set -q index[1]
		set -e argv[$index]
	else
		return 1
	end
	__fish_atools_get_aports $argv
end
