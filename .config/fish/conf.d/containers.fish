if status --is-interactive
	if test -n "$TOOLBOX_PATH"
  	  set --global --export BROWSER 'flatpak-spawn --host flatpak --user run org.mozilla.firefox'
	end
end
