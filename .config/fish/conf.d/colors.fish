# Set all the colors for our colorscheme
set -U fish_color_autosuggestion brblack
set -U fish_color_cancel --reverse
set -U fish_color_command 000000
set -U fish_color_comment 777777
set -U fish_color_cwd green
set -U fish_color_cwd_root red
set -U fish_color_end 000000
set -U fish_color_error BCBCCD
set -U fish_color_escape 008080
set -U fish_color_history_current --bold
set -U fish_color_host normal
set -U fish_color_host_remote yellow
set -U fish_color_match black --background=white
set -U fish_color_normal normal
set -U fish_color_operator 008080
set -U fish_color_param 2e2d2d
set -U fish_color_quote 333333
set -U fish_color_redirection 777777
set -U fish_color_search_match black --background=white
set -U fish_color_selection black --background=f7f3e3
set -U fish_color_status red
set -U fish_color_user 216609
set -U fish_color_valid_path --underline
set -U fish_pager_color_completion black
set -U fish_pager_color_description brblack
set -U fish_pager_color_prefix black --bold --underline
set -U fish_pager_color_progress ffffff --background=008080
