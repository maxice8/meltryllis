# Add our scripts and our flatpaks
fish_add_path --global --prepend ~/bin ~/.local/share/flatpak/exports/bin

# Add go tools
fish_add_path --global --append ~/go/bin

# For Node
fish_add_path --global --append ./node_modules/.bin

# For other tools
fish_add_path --global --append ~/.local/bin

# For Rust!
fish_add_path --global --append ~/.cargo/bin
