# If we have nvim then use it
if command -q nvim
    set --global --export EDITOR nvim
    set --global --export VISUAL nvim
end

set --global --export PAGER less
