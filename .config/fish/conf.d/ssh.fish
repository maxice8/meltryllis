if status --is-interactive
	if test -n "$SSH_CLIENT"
	    if test (echo -n "$SSH_CLIENT" | string split ' ' | tail -1) = 2222
	        set --global --export BROWSER 'flatpak-spawn --host firefox'
	    end
	end
end
