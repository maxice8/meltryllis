# NPROC variable is used by 'mk' to define the number of tasks
# that can be done at once, set it to how many cores we have
set --global --export NPROC (nproc)

# Set APORTSDIR and MANPAGER
set --global --export APORTSDIR "$HOME"/Repositories/aports
