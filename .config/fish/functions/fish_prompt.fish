function fish_prompt
    if test "$SSH_CLIENT"
        set_color black; echo -n "["
        set_color blue; echo -n "ssh"
        set_color black; echo -n "::"
        set_color cyan
        if test (echo -n "$SSH_CLIENT" | string split ' ' | tail -1) = 2222
            echo -n "toolbox"
        else if test (echo -n "$SSH_CLIENT" | string split ' ' | tail -1) = 2223
            echo -n "alpine"
        else if test (echo -n "$SSH_CLIENT" | string split ' ' | tail -1) = 22
            echo -n "host"
        else
            echo -n (echo -n $SSH_CLIENT | string split ' ' | tail -1)
        end
        set_color black; echo -n "] "
    else if test -n "$TOOLBOX_PATH"
        set_color black; echo -n "["
        set_color magenta; echo -n "container"
        set_color black; echo -n "::"
        set_color cyan; echo -n "toolbox"
        set_color black; echo -n "] "
    end
    set_color black
    echo -n "───── "
    set_color black
end
