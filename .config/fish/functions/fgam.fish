# Defined in - @ line 1
function fgam --wraps='gam --no-verify' --description 'alias fgam gam --no-verify'
  gam --no-verify $argv;
end
